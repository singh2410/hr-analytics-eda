#!/usr/bin/env python
# coding: utf-8

# # EDA HR Analytics: Job Change of Data Scientists
# #By- Aarush Kumar
# #Dated: July 28,2021

# In[18]:


import numpy as np 
import pandas as pd 
import seaborn as sns 
import warnings
warnings.filterwarnings('ignore')
from pandas_profiling import ProfileReport
from matplotlib import pyplot as plt


# In[19]:


train_data=pd.read_csv("/home/aarush100616/Downloads/Projects/EDA HR Analytics/aug_train.csv")
test_data=pd.read_csv("/home/aarush100616/Downloads/Projects/EDA HR Analytics/aug_test.csv")


# In[20]:


print(train_data.shape)
print(test_data.shape)


# In[21]:


print("columns of train data :\n" ,train_data.columns)
print("-------------------------------------------------------")
print("columns of test data :\n",test_data.columns )


# In[22]:


print('Missing values in train dataset:\n\n', train_data.isnull().sum())
print("------------------------------------------")
print('\n\nMissing values in test dataset:\n\n', test_data.isnull().sum())


# In[23]:


profile_train_data=ProfileReport(train_data , title="Job Change of Data Scientists training_data profiling report ")
profile_test_data=ProfileReport(train_data , title="Job Change of Data Scientists testing_data profiling report ")


# In[24]:


profile_train_data


# In[25]:


profile_test_data


# In[26]:


train_data.head(10)


# In[27]:


test_data.head(10)


# In[28]:


train_data.info()


# In[29]:


test_data.info()


# In[30]:


# Top 15 features with missing data for train data 
sns.set_style("whitegrid")
plt.style.use('fivethirtyeight')
plt.figure(figsize=(15,4))
df=pd.Series(1 - train_data.count() / len(train_data)).sort_values(ascending=False).head(20)
sns.barplot(x=df.index, y=df,palette="Blues_d")
plt.xticks(rotation=90)


# In[31]:


sns.heatmap(train_data.isnull(),cbar=False, cmap='viridis')


# In[32]:


# Top 5 features with missing data for test data 

sns.set_style("whitegrid")
plt.style.use('fivethirtyeight')
plt.figure(figsize=(15,4))
df=pd.Series(1 - test_data.count() / len(test_data)).sort_values(ascending=False).head(20)
sns.barplot(x=df.index, y=df,palette="Blues_d")
plt.xticks(rotation=90)


# In[33]:


sns.heatmap(test_data.isnull(),cbar=False, cmap='viridis')


# In[34]:


#missing data for train data 
total = train_data.isnull().sum().sort_values(ascending=False)
percent = (train_data.isnull().sum()/train_data.shape[0]).sort_values(ascending=False)
missin_train = pd.concat([total, percent], axis=1, keys=['Total', 'Perc_missing'])
missin_train.head(10)


# In[35]:


#missing data for train data 
total = test_data.isnull().sum().sort_values(ascending=False)
percent = (test_data.isnull().sum()/test_data.shape[0]).sort_values(ascending=False)
missing_test = pd.concat([total, percent], axis=1, keys=['Total', 'Perc_missing'])
missing_test.head(10)


# In[36]:


train_data["gender"].dropna(inplace=True)
train_data["company_type"].dropna(inplace=True)
train_data["company_size"].dropna(inplace=True)
train_data.drop(['enrollee_id'], axis = 1, inplace = True)
train_data.dropna(inplace=True)


# In[37]:


train_data.isnull().sum()


# In[38]:


print(list(train_data.columns))
print("------------------------------------------------------")
print(train_data.shape)


# In[39]:


test_data.dropna(inplace=True)
test_data.drop(['enrollee_id'], axis = 1, inplace = True)


# In[40]:


test_data.isnull().sum()


# In[41]:


print(list(test_data.columns))
print("--------------------------------------------------")
print(test_data.shape)


# In[42]:


train_data['experience'].value_counts()


# In[43]:


def replacment(experience):
    if experience == '>20':
        return 21
    elif experience == '<1':
        return 0

    else:
        return experience


# In[44]:


train_data.experience = train_data.experience.map(replacment)


# In[45]:


train_data['experience'].value_counts()


# In[46]:


train_data['last_new_job'].unique()


# In[47]:


train_data['last_new_job'].value_counts()


# In[48]:


# We will use the same function.
def replacement_2(last_new_job):
    if last_new_job == '>4':
        return 5
    elif last_new_job == 'never':
        return 0

    else:
        return last_new_job
train_data.last_new_job = train_data.last_new_job.map(replacement_2)


# In[49]:


train_data['last_new_job'].unique()


# In[50]:


value = train_data['target'].value_counts().values.tolist()
labels = train_data['target'].value_counts().index
plt.figure(figsize= (5,5))
plt.title('The ratio of each component to the target column.')
plt.pie(x = value, labels = labels, autopct='%1.f%%', pctdistance= .5)
plt.show()


# In[51]:


train_data.corr()


# In[52]:


# Now we will draw a heat map.
fig, ax = plt.subplots(figsize=(20,20))
sns.heatmap(train_data.corr(), annot=True, linewidths=.5, ax=ax)
plt.show()


# In[54]:


table = pd.pivot_table(train_data,index=['education_level','target','gender'])
print(table)
table.plot(kind='hist',
           figsize = (15,10),
           colormap ="Dark2")


# ## To be Contd.
